# ADAMcala
A mancala game for the Coleco ADAM, with FujiNet support

## Name
ADAMcala, a mancala game for the Coleco ADAM computer.  

## Description
This project;s goal is to build a mancala game for the Coleco ADAM computer that can be played, local, against a computer opponent, or over the internet using FujiNet devices.  

## Installation
Copy the adamcala.ddp to your local TNFS server.


## Screenshots
![Alt text](/images/ADAMcala-Thom.jpg?raw=true "Optional Title")


## Usage
Mount the disk image to Device 1 in FujiNet and boot to the device. 

## Support
For support reach out to me at john@wohlershome.net, or in the Coleco Adam group on facebook, or file an issue here on Gitlab. 

## Roadmap
* Revamp remote player code to use an intermediary server and eliminate the need to open TCP port 6502. 
* Add difficulty options for Computer opponent.
* Improve sound, possibly add music
* Take advantage of the FujiNet "AppKey" functionality to store player name,and other preferences.

## Contributing
If you would like to contribute code, please reach out and let me know.   I'd love to see additional platform support as well.

## Authors and acknowledgment
Author: John Wohlers (john@wohlershome.net)

Special thanks to Thomas Cherryhomes for all of his hard work on FujiNet, and for his willingness to answer questions I had while developing the game.

Beta Testers:
* Thomas Cherryhomes
* Henry R

Gameplay Advice:
* David Franks
* Kendall Vance

