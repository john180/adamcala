/**
 * @file screens.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "font.h"
#include "screens.h"
#include "instructions.h"
#include "fujinet.h"


/**
 * @brief Draw game logo header
 * 
 * @param x 
 * @param y 
 */
void logo(int x, int y)
{
	msx_color(INK_WHITE,BOARD_COLOR,BORDER_COLOR);
	gotoxy(x,y);
	cputs("\x8B\x8C\x8D\x8E\x8F\x90\x91\x92");
	gotoxy(x,y+1);
	cputs("\x93\x94\x95\x96\x97\x98\x99\x9a");
	cputs("cala  for #FujiNet");
}


// GAME BOARD
void drawBoard(void)
{
  msx_color(INK_BLACK,7,7);
  clrscr();
  logo(3, 1);  //display logo

  // Draw the board
  msx_color(INK_DARK_BLUE,BOARD_COLOR,BORDER_COLOR);
  gotoxy(BOARD_X, BOARD_Y ); 
  cputs("\x80\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x83\x81");
  gotoxy(BOARD_X, BOARD_Y+1 ); 
  cputs("\x85                          \x86");
  gotoxy(BOARD_X, BOARD_Y+2 ); 
  cputs("\x85\x80\x83\x81\x20\x80\x83\x81\x80\x83\x81\x80\x83\x81\x80\x83\x81\x80\x83\x81\x80\x83\x81\x20\x80\x83\x81\x86");
  gotoxy(BOARD_X, BOARD_Y+3 ); 
  cputs("\x85\x85\x20\x86\x20\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x20\x85\x20\x86\x86");
  gotoxy(BOARD_X, BOARD_Y+4 ); 
  cputs("\x85\x85\x20\x86\x20\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x20\x85\x20\x86\x86");
  gotoxy(BOARD_X, BOARD_Y+5 ); 
  cputs("\x85\x85\x20\x86\x20\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x20\x85\x20\x86\x86");
  gotoxy(BOARD_X, BOARD_Y+6 ); 
  cputs("\x85\x85\x20\x86\x20\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x20\x85\x20\x86\x86");
  gotoxy(BOARD_X, BOARD_Y+7 ); 
  cputs("\x85\x85\x20\x86\x20\x82\x84\x87\x82\x84\x87\x82\x84\x87\x82\x84\x87\x82\x84\x87\x82\x84\x87\x20\x85\x20\x86\x86");
  gotoxy(BOARD_X, BOARD_Y+8 ); 
  cputs("\x85\x85\x20\x86\x20\x80\x83\x81\x80\x83\x81\x80\x83\x81\x80\x83\x81\x80\x83\x81\x80\x83\x81\x20\x85\x20\x86\x86"); // Center pit divide
  gotoxy(BOARD_X, BOARD_Y+9 ); 
  cputs("\x85\x85\x20\x86\x20\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x20\x85\x20\x86\x86");
  gotoxy(BOARD_X, BOARD_Y+10 ); 
  cputs("\x85\x85\x20\x86\x20\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x20\x85\x20\x86\x86");
  gotoxy(BOARD_X, BOARD_Y+11 ); 
  cputs("\x85\x85\x20\x86\x20\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x20\x85\x20\x86\x86");
  gotoxy(BOARD_X, BOARD_Y+12 ); 
  cputs("\x85\x85\x20\x86\x20\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x85\x20\x86\x20\x85\x20\x86\x86");
  gotoxy(BOARD_X, BOARD_Y+13 ); 
  cputs("\x85\x82\x84\x87\x20\x82\x84\x87\x82\x84\x87\x82\x84\x87\x82\x84\x87\x82\x84\x87\x82\x84\x87\x20\x82\x84\x87\x86");
  gotoxy(BOARD_X, BOARD_Y+14 ); 
  cputs("\x85                          \x86");
  gotoxy(BOARD_X, BOARD_Y+15 ); 
  cputs("\x82\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84\x87");

}  

/**
 * @brief Draws the splash / config screen
 * 
 */
void drawSplash(void)
{
	msx_color(INK_DARK_BLUE,BOARD_COLOR,BORDER_COLOR);
	clrscr();
	logo(3, 1);  //display logo
	msx_color(INK_DARK_BLUE,BOARD_COLOR,BORDER_COLOR);
	drawBox(2,3,28,8,true);
    instructions(1);
	gotoxy(3,11);
	msx_color(INK_WHITE,BOARD_COLOR,BORDER_COLOR);
	cputs("GAME TYPE");
	msx_color(INK_DARK_BLUE,BOARD_COLOR,BORDER_COLOR);
	drawBox(2,12,28,5,true);
	gotoxy(4,13);
	msx_color(INK_WHITE,BOARD_COLOR,BORDER_COLOR);	
	cputs("Computer");
	gotoxy(4,14);
	cputs("Face to Face");
	gotoxy(4,15);
	if (isFujiNet()) {
        msx_color(INK_WHITE,BOARD_COLOR,BORDER_COLOR);
    }
    else {
        msx_color(INK_GRAY,BOARD_COLOR,BORDER_COLOR);
    }
	cputs("Online");
    msx_color(INK_WHITE,BOARD_COLOR,BORDER_COLOR);
	gotoxy(3,17);
	cputs("YOUR NAME");
	gotoxy(3,19);
	cputs("_");
	msx_color(INK_DARK_BLUE,BOARD_COLOR,BORDER_COLOR);
	drawBox(2,18,28,3,true);

}

/**
 * @brief Closing credits screen
 * 
 */
void drawCredits(void) {
	msx_color(INK_DARK_BLUE,BOARD_COLOR,BORDER_COLOR);
	clrscr();
	logo(2, 4);  //display logo
	msx_color(INK_BLACK,BOARD_COLOR,BORDER_COLOR);
	drawBox(1,3,29,11,false);
	gotoxy(2, 7); 
	cputs("\x84\x84\x84\x84\x84\x84\x84\x84\x84\x84 CREDITS \x84\x84\x84\x84\x84\x84\x84\x84");
    gotoxy(2, 8); 
	cputs("Game Logic: John Wohlers");
    gotoxy(2, 9); 
	cputs("ADAM libs: Thom Cherryhomes");
    gotoxy(2, 11); 
	cputs("Version: 1.0-b1 - \x9E 2022");

}

void drawFujiOpponent(void)
{
	msx_color(INK_DARK_BLUE,BOARD_COLOR,BORDER_COLOR);
	clrscr();
	logo(3, 1);  //display logo
	msx_color(INK_DARK_BLUE,BOARD_COLOR,BORDER_COLOR);
	drawBox(2,3,28,6,true);
   	msx_color(INK_WHITE,BOARD_COLOR,BORDER_COLOR);
	gotoxy(3, 4); 
	cputs("Online play requires TCP");
	gotoxy(3, 5); 
	cputs("port 6502 forwarded to the");
	gotoxy(3, 6); 
	cputs("FujiNet device from your");
	gotoxy(3, 7); 
	cputs("firewall / router.");
	gotoxy(3, 10); 
	cputs("CONNECTION MODE");
	gotoxy(4, 12); 
	cputs("HOST");
    gotoxy(4, 13); 
	cputs("GUEST");
	     
	msx_color(INK_DARK_BLUE,BOARD_COLOR,BORDER_COLOR);
	drawBox(2,11,28,4,false);
   	msx_color(INK_WHITE,BOARD_COLOR,BORDER_COLOR);
    gotoxy(3, 16);
	cputs("OPPONENT HOSTNAME / IP");
	msx_color(INK_DARK_BLUE,BOARD_COLOR,BORDER_COLOR);
	drawBox(2,17,28,3,false);

    msx_color(INK_DARK_YELLOW,BOARD_COLOR,BORDER_COLOR);
    gotoxy(3,12);
    cputs("\x9D");

}


