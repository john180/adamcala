/**
 * @file screens.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef SCREENS_H
#define SCREENS_H

#include <stdbool.h>
#include "drawbox.h"
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>


#define BOARD_X 2     // Start column of board
#define BOARD_Y 3     // Start row of board

#define BOARD_COLOR 7    // Color for board
#define BORDER_COLOR 7   // color for border

/**
 * @brief Draws the game logo header at x,y
 * 
 * @param x 
 * @param y 
 */
void logo(int x, int y);

/**
 * @brief Draws the game board at position defined by 
 * 
 */
void drawBoard(void);


/**
 * @brief Draws the splash / config screen
 * 
 */
void drawSplash(void);


/**
 * @brief Draws closing credits screen
 * 
 */
void drawCredits(void);

/**
 * @brief Draws the opponent layout screen
 * 
 */
void drawFujiOpponent(void);

#endif /* SCREENS_H */