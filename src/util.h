/**
 * @file util.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>
#include <stdbool.h>
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <smartkeys.h>
#include <eos.h>


#define CREATOR_ID 0x574A
#define COMMON_APP_ID 0x43  //Common attributes usable across all of my programs
#define ADAMCALA_APP_ID 0x61  //AdamCala specific attributes
#define PLAYER_NAME_KEY_ID 0x61 // Player 1 name storage



void status(char *s);

void debug(int num);

char *lastNchar(const char *str, int n);

#endif /* UTIL_H */