/**
 * @file fujinet.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief Functions for fujinet game play
 * @version 0.1
 * @date 2022-02-15
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <stdlib.h>
#include <stdbool.h>
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <smartkeys.h>
#include <eos.h>
#include "util.h"
#include "screens.h"
#include "fujinet.h"
#include "adamkeys.h"


#define CONNECTED 2   // Bit 1 of returned status is whether we are connected.

#define RETRY_COUNT 128 // # of times to retry

#define READ_WRITE  12  // protocol channel needs both read and write
#define NONE         0  // Translation mode = none

unsigned char response[1024]; // Global response buffer used by recv()

/**
 * @brief Check for presense of fujinet
 * 
 * colem Emulator returned 0x00 
 * personal ADAM returned 0x80 when fujinet present
 * 
 * @return true 
 * @return false 
 */
bool isFujiNet(void) {
	DCB *dcb = eos_find_dcb(FUJINET_DEVICE);
	unsigned char status;

	eos_scan_for_devices();

	status = eos_request_device_status(FUJINET_DEVICE,dcb);

	return ((status < 0x80) ? false : true );
  
}

bool configFujiPlayer(char* hostname) {
    char c;
    bool isHost = true;

    drawFujiOpponent();
    smartkeys_display("  MODE",NULL,NULL,NULL,NULL," CONNECT");
    gotoxy(3,18);
    msx_color(INK_GRAY,BOARD_COLOR,BORDER_COLOR);                
    cputs ("\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0");
     
    while (c != SMARTKEY_VI)
	{
		gotoxy(3,17);
		c=cgetc();
		int len1 = strlen(hostname);
        if (c == SMARTKEY_I) {
            smartkeys_sound_play(SOUND_MODE_CHANGE);
            isHost = !isHost;
            msx_color(INK_DARK_YELLOW,BOARD_COLOR,BORDER_COLOR);
            if (isHost) { // Host mode, wait for connection
                gotoxy(3,12);
                cputs("\x9D");
                gotoxy(3,13);
                cputs("\x20");
                gotoxy(3,18);
                msx_color(INK_GRAY,BOARD_COLOR,BORDER_COLOR);                
                cputs ("\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0");
            }
            else {  // Guest mode, we connect to opponent
                gotoxy(3,12);
                cputs("\x20");
                gotoxy(3,13);
                cputs("\x9D");            
                gotoxy(3,18);
                msx_color(INK_BLACK,BOARD_COLOR,BORDER_COLOR);                
                cputs ("\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20");
                gotoxy(3,18);
                cputs (lastNchar(hostname,26));                
            }
        }

    /**
     * Valid Characters per RFC1034    
     *   --Technically there should be better validation here... 
     *   --According to RFC 1035 the length of a FQDN is limited to 255 characters, and each label can be no longer than 63 bytes.
     **/
		if (((c >= 'a' && c <='z')||(c >= 'A' && c <='Z') || ( c >= '0' && c <= '9') || c == '.' || c == '-') && len1 < 255 && !isHost) 
		{
			smartkeys_sound_play(SOUND_KEY_PRESS);
            hostname[len1] = c;
            hostname[len1+1] = '\0';
                msx_color(INK_BLACK,BOARD_COLOR,BORDER_COLOR);
                gotoxy(3,18);
                cputs (lastNchar(hostname,26));
		}
		if ((c == KEY_DELETE || c == KEY_BACKSPACE) && len1 > 0 && !isHost) // Backspace
		{
			smartkeys_sound_play(SOUND_TYPEWRITER_CLACK);
      hostname[len1-1] = '\0';
      gotoxy(3,18);
      msx_color(INK_BLACK,BOARD_COLOR,BORDER_COLOR);
      cputs (lastNchar(hostname,26));
      if (len1 <= 26) cputs ("\x20");
		}
  }
  return (isHost);
}


// FUJINET FUNCTIONS /////////////////////////////////////////////////////////
//     Taken from Thom Cherryhomes Connect Four Game

/**
 * Connect to host, wait for connection until timeout.
 * @param h char pointer to host name
 * @return true if connected.
 */
bool fujinetConnect(char *h)
{
  unsigned char retries=RETRY_COUNT;
  DCB *dcb = eos_find_dcb(NET);
  
  struct
  {
    char cmd;
    char mode;
    char translation;
    char url[96];
  } c;

  c.cmd = 'O';
  c.mode = READ_WRITE;
  c.translation = NONE;
  sprintf(c.url,"N:TCP://%s:6502/",h);

  eos_write_character_device(NET,(char *)c, sizeof(c));

  while (retries > 0)
    {
      csleep(5);
      
      while (eos_request_device_status(NET,dcb) < ACK); // sit and spin; Wait for status

      if (eos_get_device_status(NET) & CONNECTED) // Are we connected?
	return true;
      else
	retries--;      
    }

  return false; // Unable to connect.  
}

/**
 * @brief ask FujiNet to wait for a TCP connection on port 6502
 */
void fujinetListen(void)
{
  struct
  {
    char cmd;
    char mode;
    char translation;
    char url[16];
  } listenCmd;

  listenCmd.cmd = 'O';
  listenCmd.mode = READ_WRITE;
  listenCmd.translation = 0;
  strcpy(listenCmd.url,"N:TCP://:6502/");

  eos_write_character_device(NET,(char *)listenCmd,sizeof(listenCmd));
}

/**
 * @brief is there a connection waiting?
 * @return true if connection waiting.
 */
bool fujinetConnectionWaiting(void)
{
  DCB *dcb = eos_find_dcb(NET);
  char c;
  while (eos_request_device_status(NET,dcb) < 0x80);
  c=eos_get_device_status(NET);

  if (c==2)
    return true;
  else
    return false;
}

/**
 * @brief Accept connection
 */
void fujinetAcceptConnection(void)
{
  struct
  {
    char cmd;
    char aux1;
    char aux2;
  } a;

  a.cmd = 'A';
  a.aux1 = READ_WRITE;
  a.aux2 = NONE;
  eos_write_character_device(NET,(char *)a,sizeof(a));
}

/**
 * @brief Close connection
 */
void fujinetCloseConnection(void)
{
  struct
  {
    char cmd;
    char aux1;
    char aux2;
  } a;

  a.cmd = 'C';
  a.aux1 = NONE;
  a.aux2 = NONE;
  eos_write_character_device(NET,(char *)a,sizeof(a));
}

/**
 * @brief Send data to other host
 * @param buf The buffer to send
 */
void fujinetSend(char *buf)
{
  struct
  {
    char cmd;
    char buf[64];
  } s;

  s.cmd = 'W';

  strcpy(s.buf,buf);
  strcat(s.buf,"\n"); // Terminate with LF

  eos_write_character_device(NET,(char *)s,strlen(s.buf)+1); // To account for command byte
}

/**
 * @brief receive data from other host
 * @param buf The receiving buffer.
 */
void fujinetRecv(char *buf)
{
  char *p=NULL;
  DCB *dcb = eos_find_dcb(NET);
  
  while (eos_read_character_device(NET,response,sizeof(response)) != ACK); // We have to always ask for receive in 1024 bytes.

  strncpy(buf,response,dcb->len);

  p = strchr(buf,'\n');

  if (p!=NULL)
    *p=0; // remove EOL
}

/**
 * @brief Listen for connection on TCP port 6502 (server mode)
 */
bool fujinetListenForConnection(void)
{
  fujinetListen();

  status("  WAITING FOR CONNECTION TO TCP PORT 6502...");

  while (fujinetConnectionWaiting()==false);

  status("  CONNECTION WAITING...ACCEPTING...");

  fujinetAcceptConnection();
  
  return true;
}

/**
 * @brief connect to host pointed by n
 * @param n pointer to hostname
 * @return false = successful (as listen will be false)
 */
bool fujinetConnectToHost(char *n)
{
  bool connection_status;
  
  status("  CONNECTING TO HOST, PLEASE WAIT...");
  if (fujinetConnect(n))
    {
      status("  CONNECTION SUCCESSFUL.");
      connection_status=true;
    }
  else
    {
      status("  COULD NOT CONNECT.");
      connection_status=false;
    }
  
  sleep(1);  
  return connection_status;
}

/**
 * @brief send move to other host
 * @param pit pit played
 */
void fujinetSendPit(unsigned char sendPit)
{
  char s[4];

  sprintf(s,"%d", sendPit);
  fujinetSend(s);
}

/**
 * @brief Return a random number from the FujiNet Device.
 * 
 * @return unsigned long 
 */
unsigned long fujinetRandomNumber(void)
{
  char r=0xD3;  // Random number request code
  unsigned long *p=(unsigned long *)&response[0];  //unsigned long on ADAM is same byte length as int on esp32
  
  eos_write_character_device(FUJINET_DEVICE,&r,1); // Send request to FujiNet
  eos_read_character_device(FUJINET_DEVICE,response,1024); // Read response

  return *p;
}

unsigned char fujinetAppkeyRead(unsigned int creator, unsigned char app, unsigned char key, char *buf)
{
  unsigned char r=0;
  
  struct
  {
    unsigned char cmd;
    unsigned short creator;
    unsigned char app;
    unsigned char key;
  } a;

  a.cmd = ADAMNET_SEND_APPKEY_READ;
  a.creator = creator;
  a.app = app;
  a.key = key;

  r=eos_write_character_device(FUJINET_DEVICE,(unsigned char *)a,sizeof(a));
  if (r > 0x80)
    return r;
  else
    return eos_read_character_device(FUJINET_DEVICE,buf,1024);
}

unsigned char fujinetAppkeyWrite(unsigned int creator, unsigned char app, unsigned char key, char *buf)
{
  struct
  {
    unsigned char cmd;
    unsigned short creator;
    unsigned char app;
    unsigned char key;
    char data[64];
  } a;

  a.cmd = ADAMNET_SEND_APPKEY_WRITE;
  a.creator = creator;
  a.app = app;
  a.key = key;
  strncpy(a.data,buf,sizeof(a.data));

  return eos_write_character_device(FUJINET_DEVICE,(unsigned char *)a,sizeof(a));
}
