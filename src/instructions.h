/**
 * @file instructions.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */


#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>

/**
 * @brief Draws a given page of instructions
 * 
 * @param page 
 */
void instructions(int page);

#endif