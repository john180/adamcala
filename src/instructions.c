/**
 * @file instructions.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "instructions.h"
#include "screens.h"

/**
 * @brief Writes a page of instructions
 * 
 * @param page 
 */
void instructions(int page) {
	if (page == 1) {
		msx_color(15,BOARD_COLOR,BORDER_COLOR);
		gotoxy(3,4);
		cputs("How To Play: On your turn ");
		gotoxy(3,5);
		cputs("select a pit. Stones are  ");
		gotoxy(3,6);
		cputs("sewn one per pit counter- ");
		gotoxy(3,7);
		cputs("clockwise skipping your   ");
		gotoxy(3,8);
		cputs("opponents capture pit. If ");
		gotoxy(3,9);
		cputs("your last stone lands in \x9F");
	}
	else if (page == 2) {
		msx_color(15,BOARD_COLOR,BORDER_COLOR);
		gotoxy(3,4);
		cputs("your capture pit, you take");
		gotoxy(3,5);
		cputs("an extra turn. If your    ");
		gotoxy(3,6);
		cputs("last stone lands in an    ");
		gotoxy(3,7);
		cputs("empty pit on your side of ");
		gotoxy(3,8);
		cputs("the board, you capture the");
		gotoxy(3,9);
		cputs("opposite side's pit.      ");

	}
}
