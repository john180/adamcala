/**
 * @file font.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef FONT_H
#define FONT_H

#include <sys/ioctl.h>

/**
 * @brief Setup font
 * 
 */
void setupFont(void);



#endif /* FONT_H */