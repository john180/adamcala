/**
 * @file drawbox.c
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include <stdbool.h>
#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <stdlib.h>


/**
 * @brief Draws a box
 * 
 * @param x  // X position
 * @param y  // Y position
 * @param w  // Width
 * @param h  // Height
 * @param clear  // Fill the box with spaces?
 */
void drawBox(int x, int y, int w, int h, bool clear)
{
  int loop, fill;

  gotoxy(x, y); 
  cputs("\x80"); // top left corner
  gotoxy(x + w - 1, y); 
  cputs("\x81"); // top right corner
  gotoxy(x, y + h -1); 
  cputs("\x82"); // bottom left corner
  gotoxy(x + w - 1, y + h - 1); 
  cputs("\x87"); // bottom right corner
  for (loop=x+1; loop < (x + w - 1); loop++ ) {
	gotoxy(loop, y);
	cputs("\x83"); // top line
	gotoxy(loop, y + h -1);
	cputs("\x84"); // bottom line
  }	
  for (loop=y + 1; loop< (y + h - 1); loop++ ) {
	if (clear) {
		for (fill=(x+1); fill < (x + w - 1);fill++) {
			gotoxy( fill, loop);
			cputs("\x20"); // fill with space
		}
	}
	gotoxy(x, loop);
	cputs("\x85"); // left line
	gotoxy(x + w -1, loop);
	cputs("\x86"); // right line
  }	

}
