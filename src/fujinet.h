/**
 * @file fujinet.h
 * @author John Wohlers (john@wohlershome.net)
 * @brief 
 * @version 0.1
 * @date 2022-02-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef FUJINET_H
#define FUJINET_H

#include <smartkeys.h>
#include <eos.h>
#include "util.h"


#define FUJINET_DEVICE  0x0F
#define TAPE_1          0x08
#define NET 			0x09      // Adamnet ID for network device
#define ADAMNET_TIMEOUT 0x9B
#define STATUS_MASK     0x7F
#define ACK             0x80      // Return value for AdamNet ACK

#define ADAMNET_SEND_APPKEY_READ  0xDD
#define ADAMNET_SEND_APPKEY_WRITE 0xDE
#define MAX_APPKEY_LEN 64

/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
bool isFujiNet(void);

/**
 * @brief UI for selecting / entering opponent
 * 
 * @param game_mode 
 * @param player1_name 
 * @param player2_name 
 * @return Returns true if this copy should act as host, false if we are the guest
 */
bool configFujiPlayer(char* hostname);

/**
 * @brief returns the lastN characters os a string
 * 
 * @param str  The string to truncate
 * @param n    How many characters
 */
char *lastNchar(const char *str, int n);

/**
 * @brief 
 * 
 * @param h 
 * @return true 
 * @return false 
 */
bool fujinetConnect(char *h);

/**
 * @brief 
 * 
 */
void fujinetListen(void);

/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
bool fujinetConnectionWaiting(void);

/**
 * @brief 
 * 
 */
void fujinetAcceptConnection(void);

/**
 * @brief 
 * 
 */
void fujinetCloseConnection(void);

/**
 * @brief 
 * 
 * @param buf 
 */
void fujinetSend(char *buf);

/**
 * @brief 
 * 
 * @param buf 
 */
void fujinetRecv(char *buf);

/**
 * @brief 
 * 
 * @param n 
 * @return true 
 * @return false 
 */
bool fujinetConnectToHost(char *n);

/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
bool fujinetListenForConnection(void);

/**
 * @brief Send a move to the opponent
 * 
 * @param sendPit 
 */
void fujinetSendPit(unsigned char sendPit);

/**
 * @brief 
 * 
 * @param creator 
 * @param app 
 * @param key 
 * @param buf 
 * @return unsigned char 
 */
unsigned char fujinetAppkeyRead(unsigned int creator, unsigned char app, unsigned char key, char *buf);

/**
 * @brief 
 * 
 * @param creator 
 * @param app 
 * @param key 
 * @param buf 
 * @return unsigned char 
 */
unsigned char fujinetAppkeyWrite(unsigned int creator, unsigned char app, unsigned char key, char *buf);



#endif /* FUJINET_H */