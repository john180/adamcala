#include <msx.h>
#include <sys/ioctl.h>
#include <conio.h>
#include <smartkeys.h>
#include <eos.h>
#include <stdlib.h>
#include "font.h"
#include "instructions.h"
#include "drawbox.h"
#include "screens.h"
#include "fujinet.h"
#include "util.h"
#include "adamkeys.h"

// ATTRIBUTION ///////////////////////////////////////////////////////////////
// Many functions are taken from sample code at
// https://github.com/FujiNetWIFI/fujinet-apps/blob/master/connect-four/adam/src/main.c
// 
// Remaining code by John Wohlers john@wohlershome.net
//
// Rev 1.0 - 2022.02.xx
//

// CONSTANTS /////////////////////////////////////////////////////////////////

#define PLAYER_1     1
#define PLAYER_2     2

#define P1_CAPTURE_PIT  6
#define P2_CAPTURE_PIT  13

#define MODE_COMPUTER   0
#define MODE_LOCAL      1
#define MODE_FUJINET    2

//static const int pit_count_x[] = {25,21,18,15,12,9,6,2,6,9,12,15,18,21};
//Board play goes counter clockwise
static const int pit_count_x[] = {6,9,12,15,18,21,25,21,18,15,12,9,6,2};
static const int board_default[] = {4,4,4,4,4,4,0,4,4,4,4,4,4,0};
static const int pit_pairing_map[] = {12,11,10,9,8,7,6,5,4,3,2,1,0};

// VARIABLES
int board_ring [14];
int current_player = PLAYER_1; //Default to PLAYER_1



/**
 * @brief Config Screen gamoe option menu 
 * 
 * @param game_mode 
 */
void updateMenu(int game_mode) {
	msx_color(INK_DARK_YELLOW,BOARD_COLOR,BORDER_COLOR);
	gotoxy(3,13);
	cputs(" ");
	gotoxy(3,14);
	cputs(" ");
	gotoxy(3,15);
	cputs(" ");

	gotoxy(3,13 + game_mode);
	cputs("\x9D");

}

/**
 * @brief 
 * 
 * @param pit 
 */
void updateStoneImage(int pit) {	
	int stone_image;
	int stones;
	int max;
	int y;

	msx_color(INK_MEDIUM_RED,BOARD_COLOR,BORDER_COLOR);

	if (pit < 6) {
		y = BOARD_Y + 9;
		max = 4;		
	}
	else if (pit == P1_CAPTURE_PIT || pit == P2_CAPTURE_PIT) {
        y = BOARD_Y + 3;
		max = 10;
	}
	else if (pit > P1_CAPTURE_PIT && pit < P2_CAPTURE_PIT) {
		y = BOARD_Y + 3;
		max = 4;
	}

	stones = (board_ring[pit] > max ? max : board_ring[pit]);
	int i;
	// clear pit	
	for (i = 0; i < max; i++)
	{		
		gotoxy(pit_count_x[pit] + BOARD_X, y + i );
		cputs(" ");
	}
	// place new stones
	for (i = 0; i < stones; i++)
	{				
		stone_image = rand() % 3;
		gotoxy(pit_count_x[pit] + BOARD_X, y + i );
		if (stone_image == 0) cputs("\x88");
		if (stone_image == 1) cputs("\x89");
		if (stone_image == 2) cputs("\x8A");
	}
}

/*
 * Update player name / turn notification
 */
void updatePlayer(char *player1_name, char *player2_name) {
        
    int name_length = strlen(player1_name);

	msx_color(INK_BLACK,BOARD_COLOR,BORDER_COLOR);
	gotoxy(BOARD_X + 28 - name_length, BOARD_Y + 16);
	cprintf("%s", player1_name);

	gotoxy(BOARD_X + 1, BOARD_Y + 16);
    cprintf("%s", player2_name, BOARD_Y + 16);

	msx_color(INK_DARK_YELLOW,BOARD_COLOR,BORDER_COLOR);
	if (current_player == PLAYER_1) {		
		gotoxy(BOARD_X + 27 - name_length, BOARD_Y + 16);
		cputs("\x9D");
		gotoxy(BOARD_X, BOARD_Y + 16);
		cputs(" ");

	}
	else {
		gotoxy(BOARD_X + 27 - name_length, BOARD_Y + 16);
		cputs(" ");
		gotoxy(BOARD_X, BOARD_Y + 16);
		cputs("\x9D");
	}

}

void changePlayer(char *player1_name, char *player2_name) {
	current_player = (current_player == PLAYER_1 ? PLAYER_2:PLAYER_1);
	updatePlayer(player1_name, player2_name);
}

/** 
 * Update the display for a given pit
 */
void updatePit(int pit) {
    int y=0;
	y = (pit <= 6 ? 14 : 1) + BOARD_Y;
	
	//Restore background in case we were in double digits
	msx_color(INK_WHITE,BOARD_COLOR,BORDER_COLOR);
    gotoxy(pit_count_x[pit] + BOARD_X, y );
	cputs("  ");
    
	//Write stone count
	msx_color(INK_WHITE,INK_BLACK,INK_BLACK);
	gotoxy(pit_count_x[pit] + BOARD_X, y );
	cprintf("%d", board_ring[pit]);	
	
	updateStoneImage(pit);
}

/** 
 * Add one stone to the given pit
 */
void addStone(int pit) {
	board_ring[pit] = board_ring[pit] + 1;
	
	updatePit(pit);	
}	

/**
 * Clear the contents of a pit and return the number of stones in the pit
 */
int clearPit(int pit) {
	int stones = board_ring[pit];
	board_ring[pit] = 0;
	
	updatePit(pit);
	return (stones);
}

/** 
 * Initialize board for start of game
 */
void initBoard(void){
	
	int x = 0;
	while ( x < 14 )
	{
		board_ring[x] = board_default[x];
		updatePit(x);
		x++;
	}
}

/* Check for capture condition, and perform capture if possible */
void checkCapture(int pit){

	bool result = false;
	// Do nothing if it's the capture pit
	if (pit != P1_CAPTURE_PIT && pit != P2_CAPTURE_PIT) {
		// Only allow the appropriate pits based on player
		if (
			(current_player==PLAYER_1 && pit >= 0 && pit < P1_CAPTURE_PIT) ||
			(current_player==PLAYER_2 && pit >= 7 && pit < P2_CAPTURE_PIT)
			) 
		{
			if (board_ring[ pit_pairing_map[pit] ] > 0 && board_ring[pit] == 1) {			
				smartkeys_sound_play(SOUND_POSITIVE_CHIME);
				if (current_player==PLAYER_1) {
					board_ring[P1_CAPTURE_PIT] = board_ring[P1_CAPTURE_PIT] + board_ring[ pit_pairing_map[pit] ] + 1;
				}
				else { // Player 2
					board_ring[P2_CAPTURE_PIT] = board_ring[P2_CAPTURE_PIT] + board_ring[ pit_pairing_map[pit] ] + 1;
				}
				board_ring[ pit_pairing_map[pit] ] = 0;
				board_ring[ pit ] = 0;
				updatePit(P1_CAPTURE_PIT);
				updatePit(P2_CAPTURE_PIT);
				updatePit(pit);
				updatePit(pit_pairing_map[pit]);
			}
		}
	}
}

/* 
 * Clears a side and adds all stones to appropriate capture pit 
 */
void clearSide(int side) 
{
	int i;

	if (side == PLAYER_1) {
	   
		// clear pits	
		for (i = 0; i <= 5; i++)
		{		
			board_ring[P1_CAPTURE_PIT] = board_ring[P1_CAPTURE_PIT] + clearPit(i);
			updatePit(i);
		}

    }
    else {
		for (i = 7; i <= 12; i++)
		{		
			board_ring[P2_CAPTURE_PIT] = board_ring[P2_CAPTURE_PIT] + clearPit(i);
			updatePit(i);
		}

    }
    updatePit(P1_CAPTURE_PIT);
    updatePit(P2_CAPTURE_PIT);


}

/* 
* Check to see if any side has won
*/
int checkNoStones(void) 
{
	int i;
	int result = 0;
	int tally = 0;
		// Check player 1 side
		for (i = 0; i <= 5; i++)
		{		
			tally = tally + board_ring[i];
		}
		result = (tally == 0 ? PLAYER_1 : 0);

		// Player 1 hasn't won, check player 2
		if (result == 0) {
			tally = 0;		
			for (i = 7; i <= 12; i++)
			{		
				tally = tally + board_ring[i];
			}
			result = (tally == 0 ? PLAYER_2 : 0);
		}
		return (result);
}

/*
 * Plays the turn, placing stones from the selected
 *  pit into other pits on the board.
 *  Returns True if last pit was a capture pit (extra turn)
 */
bool playTurn(int pit) 
{
	int stones = clearPit(pit);
	bool extra_turn = false;

	while (stones > 0) {
		pit++;
		if (pit >13) {
			pit = 0;
		}
		/* skip opponents capture pit */
		if (current_player == PLAYER_1 && pit == P2_CAPTURE_PIT) {
			pit = 0;
		}
		if (current_player == PLAYER_2 && pit == P1_CAPTURE_PIT) {
			pit = 7;
		}

		addStone(pit);
		updatePit(P1_CAPTURE_PIT);
		updatePit(P2_CAPTURE_PIT);
		stones--;
	}
	checkCapture(pit);

	if (pit == P1_CAPTURE_PIT || pit == P2_CAPTURE_PIT) {
		/* extra turn */
		extra_turn = true;
		smartkeys_sound_play(SOUND_POSITIVE_CHIME);
		smartkeys_sound_play(SOUND_POSITIVE_CHIME);
		status("  Stone In Capture Pit, Extra Turn!");
	}

	return (extra_turn);

}

void updatePitKeys(void) {
	char b1[9], b2[9], b3[9], b4[9], b5[9], b6[9];

	if (current_player == PLAYER_1) {
		strcpy( b1, ((board_ring[0] > 0) ? "   PIT 1" : "") ); 
		strcpy( b2, ((board_ring[1] > 0) ? "  PIT 2" : "") ); 
		strcpy( b3, ((board_ring[2] > 0) ? "  PIT 3" : "") ); 
		strcpy( b4, ((board_ring[3] > 0) ? "  PIT 4" : "") ); 
		strcpy( b5, ((board_ring[4] > 0) ? "  PIT 5" : "") ); 
		strcpy( b6, ((board_ring[5] > 0) ? "  PIT 6" : "") ); 
	}
	else {
		strcpy( b1, ((board_ring[12] > 0) ? "   PIT 1" : "") ); 
		strcpy( b2, ((board_ring[11] > 0) ? "  PIT 2" : "") ); 
		strcpy( b3, ((board_ring[10] > 0) ? "  PIT 3" : "") ); 
		strcpy( b4, ((board_ring[9] > 0) ? "  PIT 4" : "") ); 
		strcpy( b5, ((board_ring[8] > 0) ? "  PIT 5" : "") ); 
		strcpy( b6, ((board_ring[7] > 0) ? "  PIT 6" : "") ); 
	}

	smartkeys_display( b1, b2, b3, b4, b5, b6 );
	

}



void gameLoop(int game_mode, char* player1_name, char* player2_name) {
	bool refresh_keys = true;
	int gameover = 0;
	int pit_selected = 0;
	char s[4];
	int remote_pit = 99;

    drawBoard();     //draw board	
	initBoard();
	updatePlayer(player1_name, player2_name);
	

	while(gameover == 0) {
		
		
		if (current_player == PLAYER_2 && game_mode == MODE_FUJINET) {
			smartkeys_display(NULL,NULL,NULL,NULL,NULL,NULL);
			smartkeys_status("  Waiting for oppponent...");
			remote_pit = 99;

		    // Receive data from remote player.
			fujinetRecv(s);
			if ( s[0] != '\x00' && strlen(s) > 0 ) {
				remote_pit = atoi(s);
				//debug(remote_pit);
				// Valid move?
				if (remote_pit >= 0 && remote_pit < P1_CAPTURE_PIT) {
					
					// Translate P1 pit on remote to P2 pit here
					pit_selected = remote_pit + 7;
					
					if (!playTurn(pit_selected)) changePlayer(player1_name, player2_name);
				}
			}
			else {
				//TODO: somehow notify remote to resend... or trigger a resync somehow...? or end game with failure?			
				smartkeys_status("  A communication error occured");
				csleep(1000);
				changePlayer(player1_name, player2_name);
			}
		
		}
		else if (current_player == PLAYER_2 && game_mode == MODE_COMPUTER) {
			// TODO: Add code to check if a pit is in danger of capturing, and give a slight random chance of defending that pit by playing it
			// Have choices for bot level?
			smartkeys_display(NULL,NULL,NULL,NULL,NULL,NULL);
			smartkeys_status("  Thinking...");

			int bot_pit = 0;				
			int x = 0;	
			do {
				bot_pit=0;
			
				// Dummmy wait	
				while (x<30000) {
					x++;
				}
				x = 0;
				while (x < P1_CAPTURE_PIT) { // Bot avoids captures ocassionally
					// calculate players moves ending pits
					int end_pit = x + board_ring[x];
					end_pit = (end_pit > P2_CAPTURE_PIT) ? end_pit - P2_CAPTURE_PIT - 1 : end_pit;
					if (
						end_pit >= 0 &&
						end_pit < P1_CAPTURE_PIT &&
						board_ring[ pit_pairing_map[end_pit] ] > 0 && 
						board_ring[end_pit] == 0 &&
						board_ring[x] > 0 
						)
					{
						// bot pieces are in danger of being captured
						int perception = rand() % 100 + 1;
						//debug(perception);
						if (perception > 50 ) {  // adjust this number for difficulty
							// The bot noticed...
							bot_pit = pit_pairing_map[end_pit];
							x = P1_CAPTURE_PIT;
						}
					}					
					x++;
				}				
				x = P2_CAPTURE_PIT -1;				
				while (x > P1_CAPTURE_PIT && bot_pit == 0) { // Bot always favors extra turn
					bot_pit = (x + board_ring[x] == P2_CAPTURE_PIT )?x:0; 
					x--;
				}
				x = P2_CAPTURE_PIT -1;
				while (x > P1_CAPTURE_PIT && bot_pit == 0) { // Bot always favors capture
					int end_pit = x + board_ring[x];
					end_pit = (end_pit > P2_CAPTURE_PIT) ? end_pit - P2_CAPTURE_PIT - 1 : end_pit;
					if (
						end_pit > P1_CAPTURE_PIT &&
						end_pit < P2_CAPTURE_PIT &&
						board_ring[ pit_pairing_map[end_pit] ] > 0 && 
						board_ring[end_pit] == 0 &&
						board_ring[x] > 0 
						)
					{
						//debug(x);
						bot_pit = x;
					}
					x--;
				}
				if (bot_pit == 0) { // Bot doesn't have a capture, go random
					bot_pit = (rand() % 6) + 7;
					while (board_ring[bot_pit] == 0) {
						bot_pit = (rand() % 6) + 7;
					}
				}
				
			} while(playTurn(bot_pit) && !checkNoStones());
			changePlayer(player1_name, player2_name);

		}
		else {
			//Run this code while it's the local players turn
			if (refresh_keys) {
				updatePitKeys();
			}
			//unsigned char k = eos_read_keyboard(); //Trying to avoid buffering of keys...
			unsigned char k = cgetc();
			refresh_keys=true;

			switch(k)
			{
				case SMARTKEY_I:
					pit_selected = (current_player == PLAYER_1) ? 0:12;
					break;
				case SMARTKEY_II:
					pit_selected = (current_player == PLAYER_1) ? 1:11;
					break;
				case SMARTKEY_III:
					pit_selected = (current_player == PLAYER_1) ? 2:10;
					break;
				case SMARTKEY_IV:
					pit_selected = (current_player == PLAYER_1) ? 3:9;
					break;
				case SMARTKEY_V:
					pit_selected = (current_player == PLAYER_1) ? 4:8;
					break;
				case SMARTKEY_VI:
					pit_selected = (current_player == PLAYER_1) ? 5:7;
					break;
				default:
					pit_selected = 99; // Invalid Key Pressed
					refresh_keys = false;
			
			}
			if (pit_selected != 99 && board_ring[pit_selected] > 0) {
				smartkeys_sound_play(SOUND_CONFIRM);	

				// Send the move to the opponent
				if (current_player == PLAYER_1 && game_mode == MODE_FUJINET) {
					fujinetSendPit(pit_selected); 
				}

				// Play move locally, if not a capture pit ending, change player
				if (!playTurn(pit_selected)) changePlayer(player1_name, player2_name);

			}
			else {
				smartkeys_sound_play(SOUND_NEGATIVE_BUZZ);
			}

		}
		gameover = checkNoStones();		
	}
	if (gameover == PLAYER_1 ) {
		clearSide(PLAYER_2);
	}
	else {
		clearSide(PLAYER_1);
	}

	char winfmt[]="\n  %s WINS!  PLAY AGAIN?";
  	char winmsg[40];

	if (board_ring[P1_CAPTURE_PIT] > board_ring[P2_CAPTURE_PIT])
	{
		sprintf(winmsg,winfmt,player1_name);
	} else if (board_ring[P1_CAPTURE_PIT] == board_ring[P2_CAPTURE_PIT]) {
		sprintf(winmsg,"  YOU TIED!  PLAY AGAIN?");		
	}
	 else
	{
		sprintf(winmsg,winfmt,player2_name);
	}
    smartkeys_display(NULL,NULL,NULL,NULL,"  YES","   NO");
	smartkeys_status(winmsg);

}

/**
 * @brief 
 * 
 * @param player_name 
 */
void displayConfigPlayerName(char* player_name) {
	gotoxy(3,19);
	msx_color(INK_BLACK,BOARD_COLOR,BORDER_COLOR);
	cputs ("          ");  // clear player name 
	gotoxy(3,19);
	cputs (player_name);

}

void displayConfigPlayerHeader(char field_label[]) {
	gotoxy(3,17);
	msx_color(INK_WHITE,BOARD_COLOR,BORDER_COLOR);
	cputs ("                  ");  // clear label
	gotoxy(3,17);
	cputs (field_label);
}

/**
 * @brief Sets the smartkeys depending on the game mode.
 * 
 * @param game_mode 
 */
void gameModeKeys(int *game_mode) {
	if (game_mode == MODE_LOCAL) {
		smartkeys_display("    GAME\n    MODE","PLAYER\n  NAME",NULL,NULL,NULL," START\n  GAME");
	}
	if (game_mode == MODE_FUJINET) {
		smartkeys_display("    GAME\n    MODE",NULL,NULL,NULL,NULL," CHOOSE\n OPPONENT");
	}
	if (game_mode == MODE_COMPUTER) {
		smartkeys_display("    GAME\n    MODE",NULL,NULL,NULL,NULL," START\n  GAME");
	}
}

/**
 * @brief Get the Stored Player Name 
 * 
 */
void getStoredPlayerName( char* player1_name ) {
	//TODO: Fix this mess...  
	unsigned char r = fujinetAppkeyRead(CREATOR_ID, COMMON_APP_ID, PLAYER_NAME_KEY_ID, response);

	if ( r == 0x80 ) 
	{
		strncpy(player1_name, response, 10);
		int len1 = strlen(player1_name); // make sure we are null terminated in case of a long player name
		player1_name[len1+1] = 0;
	}
	
  	
}


void configGame(int* game_mode, char* player1_name, char* player2_name) {
  char c;  
  int player_toggle = PLAYER_1;
  int inst_page = 1;

  updateMenu(*game_mode);
  if (isFujiNet()) {
  	getStoredPlayerName(player1_name);
  }
  displayConfigPlayerName(player1_name);

  gameModeKeys(*game_mode);
  while (c != SMARTKEY_VI || (player1_name[0] == '\0'))
	{
		gotoxy(3,19);
		c=cgetc();
		int len1 = strlen(player1_name);
		int len2 = strlen(player2_name);

		if (c == SMARTKEY_I) {
			smartkeys_sound_play(SOUND_MODE_CHANGE);
			// Change Game Mode
			(*game_mode)++;

			// Use isFujinet() to skip the Online menu item if FujiNet not present
			if (!isFujiNet() && *game_mode == MODE_FUJINET) {
				(*game_mode)++;
			}


			if (*game_mode > 2) *game_mode=0;  // loop the menu ring

			/* Update the menu indicator pip */
			updateMenu(*game_mode);

			/* Update smart keys labels */
			gameModeKeys(*game_mode);

			/* update other UI elements */
			if (*game_mode == MODE_FUJINET || *game_mode == MODE_COMPUTER) {
				displayConfigPlayerHeader("YOUR NAME");
				displayConfigPlayerName(player1_name);
			}
		}

		if (c == SMARTKEY_II && *game_mode==MODE_LOCAL) {
			smartkeys_sound_play(SOUND_MODE_CHANGE);
			if (player_toggle == PLAYER_1) {
				player_toggle = PLAYER_2;
				displayConfigPlayerHeader("PLAYER 2 NAME");
				displayConfigPlayerName(player2_name);

			}
			else {
				player_toggle = PLAYER_1;
				displayConfigPlayerHeader("PLAYER 1 NAME");
				displayConfigPlayerName(player1_name);

			}
			
		}
		if (((c >= 'a' && c <='z')||(c >= 'A' && c <='Z') || c == ' ' || c == '.' || c == '-') && ((player_toggle == PLAYER_1) ? len1 : len2) < 10) // Limit to valid keys
		{
			smartkeys_sound_play(SOUND_KEY_PRESS);
			if (player_toggle == PLAYER_1) {
	        	player1_name[len1] = c;
        		player1_name[len1+1] = '\0';
				displayConfigPlayerName(player1_name);
			}
			else {
	        	player2_name[len2] = c;
        		player2_name[len2+1] = '\0';
				displayConfigPlayerName(player2_name);
			}
		}
		if (c == KEY_DELETE || c == KEY_BACKSPACE) // Backspace
		{
			smartkeys_sound_play(SOUND_TYPEWRITER_CLACK);
			if (player_toggle == PLAYER_1) {

				if (len1 > 0) {
					player1_name[len1-1] = '\0';
					gotoxy(3,19);
					msx_color(INK_BLACK,BOARD_COLOR,BORDER_COLOR);
					cputs (player1_name);
					cputs ("\x20");
				}
			}
			else {
				if (len2 > 0) {
					player2_name[len2-1] = '\0';
					gotoxy(3,19);
					msx_color(INK_BLACK,BOARD_COLOR,BORDER_COLOR);
					cputs (player2_name);
					cputs ("\x20");
				}

			}
		}
		if (c == KEY_DOWN_ARROW) // Down Arrow
		{
			inst_page++;
			if (inst_page > 2) inst_page=2;
			instructions(inst_page);
		}
		if (c == KEY_UP_ARROW) // Up Arrow
		{
			inst_page--;
			if (inst_page < 1) inst_page=1;
			instructions(inst_page);
		}

		if (c == SMARTKEY_VI && player1_name[0] == '\0') {  // Alert on player name field empty
			smartkeys_sound_play(SOUND_NEGATIVE_BUZZ);
			status("  Please enter your name");
			csleep(300);
			gameModeKeys(*game_mode);
		}

	}
}


/**
 * @brief Program entry point
 */
void main(void)
{	
	char hostname[256];
	char player1_name[16], player2_name[16];
	int game_mode=MODE_COMPUTER; 
	char c=SMARTKEY_V;
	bool isHost;
	bool isConnected;

	//TODO: Debug FujiNet Player 2 code.

	memset(hostname,0,sizeof(hostname));
  	memset(player1_name,0,sizeof(player1_name));
  	memset(player2_name,0,sizeof(player2_name));

	strcpy(player1_name, "Player 1");
	//getStoredPlayerName(player1_name);
	
	strcpy(player2_name, "Player 2");

	if (isFujiNet()) {
		srand(fujinetRandomNumber());  // Seed with random number from fujinet
	}
	else {
		srand(123456);   // Not so random seed...
	}

    setupFont();
    smartkeys_set_mode();
	smartkeys_sound_init();	
   
	drawSplash();	
	configGame(&game_mode, player1_name, player2_name);
	if (game_mode == MODE_FUJINET) {
		//debug( isFujiNet());
		isHost = configFujiPlayer(hostname);

		if (isHost) {
			isConnected = fujinetListenForConnection();  // Host is player 1
			current_player = PLAYER_1;
		}
		else{
			isConnected = fujinetConnectToHost(hostname);  // Guest  is player 2
			current_player = PLAYER_2;
		}
		if (player1_name[0] == "\x00" ) {
			strcpy(player1_name, "Player 1");
		}
		fujinetSend(player1_name);
		fujinetRecv(player2_name);
	}
	if (game_mode == MODE_COMPUTER) {
		strcpy(player2_name, "ADAM");
	}

	// Save player1 name to Appkey
	fujinetAppkeyWrite(CREATOR_ID, COMMON_APP_ID, PLAYER_NAME_KEY_ID, player1_name);

	// TODO: Allow return to main menu to choose a different game mode
	// TODO: Allow reboot into FujiNet config program
	while (c == SMARTKEY_V)
	{
		gameLoop(game_mode, player1_name, player2_name);
		c=cgetc();
	}

	// Clean up the networking so FujiNet isn't stuck with an open port.	
	fujinetCloseConnection();

	drawCredits();
	status("    Press Any Key To Exit");	
	c=cgetc();
	eos_exit_to_smartwriter();
}

